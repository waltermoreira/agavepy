=======
AgavePy
=======

A very basic object to interact with the `Agave API`_.


.. _Agave API: http://agaveapi.co/


License
=======

MIT
